/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 *
 * @author Natasha Yulian
 */
public class LoginPertama extends JFrame implements KeyListener{

    public LoginPertama () {
        initComponents();
    }
    
    private void initComponents(){
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        //in frame
        this.setBounds(600, 600, 600, 600);
        setLocationRelativeTo(null);
        setTitle("Wumpus Game");
        String begronURL = "bahan-bahan\\Image\\begronawal.png";
        Image begron = resizeImage(begronURL,600,600);
        setContentPane(new JLabel(new ImageIcon(begron)));
        addKeyListener(this);
        BlinkingText blinkText = new BlinkingText();
        this.add(blinkText);
        Thread t1 = new Thread(blinkText);
        t1.start();
        playSound();
        
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode() == KeyEvent.VK_ENTER){
            setVisible(false);
            LoginKedua kedua = new LoginKedua();
            kedua.setVisible(true);
            }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        
    }
    
    public Image resizeImage(String url, int w, int h){
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance (w,h, Image.SCALE_SMOOTH);
        } catch (IOException ex){
            ex.printStackTrace(System.err);
        }
        return dimg;
    }
    
    static Clip clip;
    public static void playSound() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bahan-bahan\\music\\Don't Starve OST - Ragtime.wav").getAbsoluteFile());
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    
}
