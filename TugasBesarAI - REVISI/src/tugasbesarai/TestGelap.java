/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import static tugasbesarai.MainAI.bkgImg;

/**
 *
 * @author natan
 */
public class TestGelap {

    String urlDark = "bahan-bahan\\Image\\effect\\darkTile.png";
    String urlDarkR = "bahan-bahan\\Image\\effect\\darkTileR.png";

    public TestGelap() {
        initComponents();

             
    }

    private Image resizeImg(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);

        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        return dimg;
    }

    private void initComponents() {
        
        //BARIS-1
        dark1 = new JLabel();
        dark1.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark1.setSize(130, 130);
        dark1.setLocation(0, 0);
        bkgImg.add(dark1);

        dark2 = new JLabel();
        dark2.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark2.setSize(130, 130);
        dark2.setLocation(130, 0);
        bkgImg.add(dark2);

        dark3 = new JLabel();
        dark3.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark3.setSize(130, 130);
        dark3.setLocation(260, 0);
        bkgImg.add(dark3);

        dark4 = new JLabel();
        dark4.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark4.setSize(130, 130);
        dark4.setLocation(390, 0);
        bkgImg.add(dark4);

        //BARIS 2
        dark5 = new JLabel();
        dark5.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark5.setSize(130, 130);
        dark5.setLocation(0, 130);
        bkgImg.add(dark5);

        dark6 = new JLabel();
        dark6.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark6.setSize(130, 130);
        dark6.setLocation(130, 130);
        bkgImg.add(dark6);

        dark7 = new JLabel();
        dark7.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark7.setSize(130, 130);
        dark7.setLocation(260, 130);
        bkgImg.add(dark7);

        dark8 = new JLabel();
        dark8.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark8.setSize(130, 130);
        dark8.setLocation(390, 130);
        bkgImg.add(dark8);
        
        //BARIS-3
        
        dark9 = new JLabel();
        dark9.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark9.setSize(130, 130);
        dark9.setLocation(0, 260);
        bkgImg.add(dark9);

        dark10 = new JLabel();
        dark10.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark10.setSize(130, 130);
        dark10.setLocation(130, 260);
        bkgImg.add(dark10);

        dark11 = new JLabel();
        dark11.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark11.setSize(130, 130);
        dark11.setLocation(260, 260);
        bkgImg.add(dark11);

        dark12 = new JLabel();
        dark12.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark12.setSize(130, 130);
        dark12.setLocation(390, 260);
        bkgImg.add(dark12);
        
        //baris 4
        
        dark13 = new JLabel();
        dark13.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark13.setSize(130, 130);
        dark13.setLocation(0, 390);
        bkgImg.add(dark13);

        dark14 = new JLabel();
        dark14.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark14.setSize(130, 130);
        dark14.setLocation(130, 390);
        bkgImg.add(dark14);

        dark15 = new JLabel();
        dark15.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark15.setSize(130, 130);
        dark15.setLocation(260, 390);
        bkgImg.add(dark15);

        dark16 = new JLabel();
        dark16.setIcon(new ImageIcon(resizeImg(urlDark, 130, 130)));
        dark16.setSize(130, 130);
        dark16.setLocation(390, 390);
        bkgImg.add(dark16);
        
        //--REVERSE-------------------------------------------------------------
        //BARIS-1
        dark1r = new JLabel();
        dark1r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark1r.setSize(130, 130);
        dark1r.setLocation(0, 0);
        bkgImg.add(dark1r);

        dark2r = new JLabel();
        dark2r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark2r.setSize(130, 130);
        dark2r.setLocation(130, 0);
        bkgImg.add(dark2r);

        dark3r = new JLabel();
        dark3r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark3r.setSize(130, 130);
        dark3r.setLocation(260, 0);
        bkgImg.add(dark3r);

        dark4r = new JLabel();
        dark4r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark4r.setSize(130, 130);
        dark4r.setLocation(390, 0);
        bkgImg.add(dark4r);

        //BARIS 2
        dark5r = new JLabel();
        dark5r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark5r.setSize(130, 130);
        dark5r.setLocation(0, 130);
        bkgImg.add(dark5r);

        dark6r = new JLabel();
        dark6r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark6r.setSize(130, 130);
        dark6r.setLocation(130, 130);
        bkgImg.add(dark6r);

        dark7r = new JLabel();
        dark7r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark7r.setSize(130, 130);
        dark7r.setLocation(260, 130);
        bkgImg.add(dark7r);

        dark8r = new JLabel();
        dark8r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark8r.setSize(130, 130);
        dark8r.setLocation(390, 130);
        bkgImg.add(dark8r);
        
        //BARIS-3
        
        dark9r = new JLabel();
        dark9r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark9r.setSize(130, 130);
        dark9r.setLocation(0, 260);
        bkgImg.add(dark9r);

        dark10r = new JLabel();
        dark10r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark10r.setSize(130, 130);
        dark10r.setLocation(130, 260);
        bkgImg.add(dark10r);

        dark11r = new JLabel();
        dark11r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark11r.setSize(130, 130);
        dark11r.setLocation(260, 260);
        bkgImg.add(dark11r);

        dark12r = new JLabel();
        dark12r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark12r.setSize(130, 130);
        dark12r.setLocation(390, 260);
        bkgImg.add(dark12r);
        
        //baris 4

        dark14r = new JLabel();
        dark14r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark14r.setSize(130, 130);
        dark14r.setLocation(130, 390);
        bkgImg.add(dark14r);

        dark15r = new JLabel();
        dark15r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark15r.setSize(130, 130);
        dark15r.setLocation(260, 390);
        bkgImg.add(dark15r);

        dark16r = new JLabel();
        dark16r.setIcon(new ImageIcon(resizeImg(urlDarkR, 130, 130)));
        dark16r.setSize(130, 130);
        dark16r.setLocation(390, 390);
        bkgImg.add(dark16r);

    }

    //baris pertama
    public JLabel dark1;
    public JLabel dark2;
    public JLabel dark3;
    public JLabel dark4;

    //baris kedua
    public JLabel dark5;
    public JLabel dark6;
    public JLabel dark7;
    public JLabel dark8;

    //baris ketiga
    public JLabel dark9;
    public JLabel dark10;
    public JLabel dark11;
    public JLabel dark12;

    //baris keempat
    public JLabel dark13;
    public JLabel dark14;
    public JLabel dark15;
    public JLabel dark16;
    
    //--reverse-----------------------------------------------------------------
    public JLabel dark1r;
    public JLabel dark2r;
    public JLabel dark3r;
    public JLabel dark4r;

    //baris kedua
    public JLabel dark5r;
    public JLabel dark6r;
    public JLabel dark7r;
    public JLabel dark8r;

    //baris ketiga
    public JLabel dark9r;
    public JLabel dark10r;
    public JLabel dark11r;
    public JLabel dark12r;

    //baris keempat
    
    public JLabel dark14r;
    public JLabel dark15r;
    public JLabel dark16r;
    

}
