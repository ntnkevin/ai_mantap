/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 *
 * @author Natasha Yulian
 */
public class FrameMenang extends JFrame {

    public FrameMenang() {
        initComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        //in frame
        this.setBounds(600, 600, 600, 600);
        setLocationRelativeTo(null);
        setTitle("Wumpus Game");
        BlinkingWin blinkWin = new BlinkingWin();
        this.add(blinkWin);
        Thread t1 = new Thread(blinkWin);
        t1.start();
    }

}
