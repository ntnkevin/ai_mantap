/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

/**
 *
 * @author user
 */
public class Kotak {
    
    private int x;
    private int y;
    private String status;
    public int [] tetangga;
    private int priority;
    private boolean visited;

    public Kotak(int locX, int locY, int i){
        this.x = locX;
        this.y = locY;
        
        if(i == 0){
            tetangga = new int[2];
            tetangga[0] = 1;
            tetangga[1] = 4;
        } else if(i == 1 || i == 2){
            tetangga = new int[3];
            tetangga[0] = i - 1;
            tetangga[1] = i + 1;
            tetangga[2] = i + 4;
        } else if(i == 3){
            tetangga = new int[2];
            tetangga[0] = 2;
            tetangga[1] = 7;
        } else if(i == 4 || i == 8){
            tetangga = new int[3];
            tetangga[0] = i - 4;
            tetangga[1] = i + 4;
            tetangga[2] = i + 1;
        } else if(i == 7 || i == 11){
            tetangga = new int[3];
            tetangga[0] = i + 4;
            tetangga[1] = i - 4;
            tetangga[2] = i - 1;
        } else if(i == 12){
            tetangga = new int[2];
            tetangga[0] = 8;
            tetangga[1] = 13;
        } else if(i == 15){
            tetangga = new int[2];
            tetangga[0] = 11;
            tetangga[1] = 14;
        } else if(i == 13 || i == 14){
            tetangga = new int[3];
            tetangga[0] = i - 1;
            tetangga[1] = i + 1;
            tetangga[2] = i - 4;
        } else {
            tetangga = new int[4];
            tetangga[0] = i + 1;
            tetangga[1] = i - 1;
            tetangga[2] = i - 4;
            tetangga[3] = i + 4;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

}
