/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 *
 * @author natan
 */
public class MainAI extends Player implements Movement {

    public MainAI() {
        initComponents();
    }

    public void blackoutInit() {

    }

    //jarak antar blackout
    private int jarak = 10;
    private String urlDefPly = "bahan-bahan/Image/player/front_view.png";
    private final String urlWumpus = "bahan-bahan/Image/Misery_Toadstool.png";
    private final String urlGold = "bahan-bahan/Image/gold2.png";
    private final String urlPit = "bahan-bahan/Image/hole.png";
    private final String urlStench = "bahan-bahan/Image/Poop_Emoji.png";
    private final String urlBreeze = "bahan-bahan/Image/tornado.png";
    private final String urlFence = "bahan-bahan/Image/fence.png";
    private static int playerLocX = 20;
    private static int playerLocY = 410;
    public static int playerLoc = 3;

    LinkedList<Integer> listChosen = new LinkedList<>();
    LinkedList<Kotak> listKotak = new LinkedList<>();

    Random rand = new Random();

    TestGelap tg;
    //NB: DI LINE 205 GW NYALAINNYA

    //buat data rulesnya
    KnowledgeLocation kl = new KnowledgeLocation();

    //forward chaining
    public static String tell = "stench=>wumpus;breeze=>pit";

    private void randomPosisiObs() {
        //random posisi obstacle (wumpus, pit, gold)
        int counter = 0;
        for (int i = 20; i < 420; i += 130) {
            for (int j = 20; j < 420; j += 130) {
                Kotak box = new Kotak(i, j, counter);
                listKotak.add(box);
                counter++;
            }
        }
        
        int chosen;
        for(int i = 0 ; i < 3 ; i++){
            chosen = rand.nextInt(15);
            
            if(listChosen.contains(chosen) || chosen == 2 || chosen == 3 || chosen == 6 || chosen == 7){
                do{
                    chosen = rand.nextInt(16);
                } while(listChosen.contains(chosen) || chosen == 2 || chosen == 3 || chosen == 6 || chosen == 7);
            }
            listChosen.add(chosen);
        }
        
        /*
            untuk urutan list chosen (index) :
            0 - wumpus
            1 - gold
            2 s.d 4 - pit
        */
    }

    private boolean isBesideEmpty(int index) {
        boolean status0 = true;
        boolean status2 = true;
        boolean status3 = true;
        boolean status4 = true;
        boolean result;
        
        if (listChosen.get(0) == index) {
            status0 = false;
        }
        if (listChosen.get(2) == index) {
            status2 = false;
        }
//        if (listChosen.get(3) == index) {
//            status3 = false;
//        }
//        if (listChosen.get(4) == index) {
//            status4 = false;
//        }

        if(status0 == true && status2 == true){
            result = true;
        } else {
            result = false;
        }
        
        return result;
    }

    private ArrayList<Integer> addConstraints(int index) {
        ArrayList<Integer> arrConstPos = new ArrayList<>();
        
        if(index == 0){
            if(isBesideEmpty(index + 1)){
                arrConstPos.add(index + 1);
            }
            if(isBesideEmpty(index + 4)){
                arrConstPos.add(index + 4);
            }
        } else if(index == 12){
            if(isBesideEmpty(index - 4)){
                arrConstPos.add(index - 4);
            }
            if(isBesideEmpty(index + 1)){
                arrConstPos.add(index + 1);
            }
        } else if(index == 15){
            if(isBesideEmpty(index - 4)){
                arrConstPos.add(index - 4);
            }
            if(isBesideEmpty(index - 1)){
                arrConstPos.add(index - 1);
            }
        } else if(index == 4 || index == 8){
            if(isBesideEmpty(index - 4)){
                arrConstPos.add(index - 4);
            }
            if(isBesideEmpty(index + 4)){
                arrConstPos.add(index + 4);
            }
            if(isBesideEmpty(index + 1)){
                arrConstPos.add(index + 1);
            }
        } else if(index == 11){
            if(isBesideEmpty(index - 4)){
                arrConstPos.add(index - 4);
            }
            if(isBesideEmpty(index + 4)){
                arrConstPos.add(index + 4);
            }
            if(isBesideEmpty(index - 1)){
                arrConstPos.add(index - 1);
            }
        } else if(index == 13 || index == 14){
            if(isBesideEmpty(index - 4)){
                arrConstPos.add(index - 4);
            }
            if(isBesideEmpty(index + 1)){
                arrConstPos.add(index + 1);
            }
            if(isBesideEmpty(index - 1)){
                arrConstPos.add(index - 1);
            }
        } else if(index == 1){
            if(isBesideEmpty(index + 4)){
                arrConstPos.add(index + 4);
            }
            if(isBesideEmpty(index + 1)){
                arrConstPos.add(index + 1);
            }
            if(isBesideEmpty(index - 1)){
                arrConstPos.add(index - 1);
            }
        } else {
            if(isBesideEmpty(index - 4)){
                arrConstPos.add(index - 4);
            }
            if(isBesideEmpty(index + 4)){
                arrConstPos.add(index + 4);
            }
            if(isBesideEmpty(index - 1)){
                arrConstPos.add(index - 1);
            }
            if(isBesideEmpty(index + 1)){
                arrConstPos.add(index + 1);
            }
        }

        return arrConstPos;
    }
    
    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        //in frame
        this.setSize(600, 600);
        setLocationRelativeTo(null);
        setTitle("Wumpus Game");

        //base color
        getContentPane().setBackground(Color.blue);

        //panel to frame
        mainPanel = new JPanel();
        add(mainPanel);

        //background ke panel
        bkgImg = new JLabel();
        bkgImg.setIcon(new ImageIcon(resizeImg("bahan-bahan/Image/Begron.jpg", 520, 520)));
        mainPanel.add(bkgImg);

        //GELAPNYA
        tg = new TestGelap();

        this.colition();

        //START
        //random posisi obstacle (wumpus, pit, gold)
        //dirandom dulu gaez sebelom di masukin ke pengetahuan
        randomPosisiObs();

        //input data knowledge
        //gw taro di class baru, soalnya biar yakin masuk engganya hehehe, jadi class pengetahuan apa yang dia tau
        kl.wumpusx = listKotak.get(listChosen.get(0)).getX();
        kl.wumpusy = listKotak.get(listChosen.get(0)).getY();

        kl.goldx = listKotak.get(listChosen.get(1)).getX();
        kl.goldy = listKotak.get(listChosen.get(1)).getY();

        kl.pit1x = listKotak.get(listChosen.get(2)).getX();
        kl.pit1y = listKotak.get(listChosen.get(2)).getY();

//        kl.pit2x = listKotak.get(listChosen.get(3)).getX();
//        kl.pit2y = listKotak.get(listChosen.get(3)).getY();
//
//        kl.pit3x = listKotak.get(listChosen.get(4)).getX();
//        kl.pit3y = listKotak.get(listChosen.get(4)).getY();

        //penempatan obstacle + constraints (breeze, stench)
        //wumpus
        wumpus = new JLabel();
        wumpus.setIcon(new ImageIcon(resizeImg(urlWumpus, 100, 100)));
        wumpus.setSize(100, 100);
        wumpus.setLocation(listKotak.get(listChosen.get(0)).getX(), listKotak.get(listChosen.get(0)).getY());
        listKotak.get(listChosen.get(0)).setStatus("wumpus");
        bkgImg.add(wumpus);
        
        //gold
        gold = new JLabel();
        gold.setIcon(new ImageIcon(resizeImg(urlGold, 50, 50)));
        gold.setSize(50, 50);
        gold.setLocation(listKotak.get(listChosen.get(1)).getX(), listKotak.get(listChosen.get(1)).getY());
        listKotak.get(listChosen.get(1)).setStatus("gold");
        bkgImg.add(gold);

        //pit
        pit1 = new JLabel();
        pit1.setIcon(new ImageIcon(resizeImg(urlPit, 100, 100)));
        pit1.setSize(100, 100);
        pit1.setLocation(listKotak.get(listChosen.get(2)).getX(), listKotak.get(listChosen.get(2)).getY());
        listKotak.get(listChosen.get(2)).setStatus("pit");
        bkgImg.add(pit1);
        
//        pit2 = new JLabel();
//        pit2.setIcon(new ImageIcon(resizeImg(urlPit, 100, 100)));
//        pit2.setSize(100, 100);
//        pit2.setLocation(listKotak.get(listChosen.get(3)).getX(), listKotak.get(listChosen.get(3)).getY());
//        listKotak.get(listChosen.get(3)).setStatus("pit");
//        bkgImg.add(pit2);
//        
//        pit3 = new JLabel();
//        pit3.setIcon(new ImageIcon(resizeImg(urlPit, 100, 100)));
//        pit3.setSize(100, 100);
//        pit3.setLocation(listKotak.get(listChosen.get(4)).getX(), listKotak.get(listChosen.get(4)).getY());
//        listKotak.get(listChosen.get(4)).setStatus("pit");
//        bkgImg.add(pit3);
        
        //ADD CONSTRAINTS
        //add stench
        ArrayList<Integer> arrHasilPosisi;
        arrHasilPosisi = addConstraints(listChosen.get(0));
        for (int i = 0; i < arrHasilPosisi.size(); i++) {
            stench = new JLabel();
            stench.setIcon(new ImageIcon(resizeImg(urlStench, 60, 60)));
            stench.setSize(60, 60);
            stench.setLocation(listKotak.get(arrHasilPosisi.get(i)).getX() + 30, listKotak.get(arrHasilPosisi.get(i)).getY() - 20);
            listKotak.get(arrHasilPosisi.get(i)).setStatus("stench");
            bkgImg.add(stench);
        }
        arrHasilPosisi.clear();
        
        //add breeze pit 1
        arrHasilPosisi = addConstraints(listChosen.get(2));
        for (int i = 0; i < arrHasilPosisi.size(); i++) {
            breeze = new JLabel();
            breeze.setIcon(new ImageIcon(resizeImg(urlBreeze, 40, 40)));
            breeze.setSize(40, 40);
            breeze.setLocation(listKotak.get(arrHasilPosisi.get(i)).getX() + 30, listKotak.get(arrHasilPosisi.get(i)).getY() + 30);
            listKotak.get(arrHasilPosisi.get(i)).setStatus("breeze");
            bkgImg.add(breeze);
        }
        arrHasilPosisi.clear();

        //add breeze pit 2
//        arrHasilPosisi = addConstraints(listChosen.get(3));
//        for (int i = 0; i < arrHasilPosisi.size(); i++) {
//            breeze = new JLabel();
//            breeze.setIcon(new ImageIcon(resizeImg(urlBreeze, 40, 40)));
//            breeze.setSize(40, 40);
//            breeze.setLocation(listKotak.get(arrHasilPosisi.get(i)).getX() + 30, listKotak.get(arrHasilPosisi.get(i)).getY() + 30);
//            listKotak.get(arrHasilPosisi.get(i)).setStatus("breeze");
//            bkgImg.add(breeze);
//        }
//        arrHasilPosisi.clear();
//
//        //add breeze pit 3
//        arrHasilPosisi = addConstraints(listChosen.get(4));
//        for (int i = 0; i < arrHasilPosisi.size(); i++) {
//            breeze = new JLabel();
//            breeze.setIcon(new ImageIcon(resizeImg(urlBreeze, 40, 40)));
//            breeze.setSize(40, 40);
//            breeze.setLocation(listKotak.get(arrHasilPosisi.get(i)).getX() + 30, listKotak.get(arrHasilPosisi.get(i)).getY() + 30);
//            listKotak.get(arrHasilPosisi.get(i)).setStatus("breeze");
//            bkgImg.add(breeze);
//        }
        arrHasilPosisi.clear();

        //player
        player = new JLabel();
        player.setIcon(new ImageIcon(resizeImg(urlDefPly, 100, 100)));
        player.setSize(100, 100);
        player.setLocation(playerLocX, playerLocY);
//        this.repaint();
        bkgImg.add(player);
        
        listKotak.get(3).setStatus("startplayer");
        listKotak.get(3).setPriority(0);
        listKotak.get(3).setVisited(true);
        
        for(int i = 0 ; i < listKotak.size() ; i++){
            if(listKotak.get(i).getStatus() == null){
                listKotak.get(i).setStatus("safe");
            }
        }
    }
    
    //image convert size
    private Image resizeImg(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);

        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        return dimg;
    }
    
    //cek wumpus pit gold
    private void tabrakanObs() {
        if (kl.pit3x == playerLocX && kl.pit3y == playerLocY) {
            playSoundObs();
            JOptionPane.showMessageDialog(null, "Masuk Pit!");
//            setVisible(false);
//            new FrameMokad().setVisible(true);

        }
        if (kl.pit2x == playerLocX && kl.pit2y == playerLocY) {
            playSoundObs();
            JOptionPane.showMessageDialog(null, "Masuk Pit!");
            setVisible(false);
            new FrameMokad().setVisible(true);

        }
        if (kl.pit1x == playerLocX && kl.pit1y == playerLocY) {
            playSoundObs();
            JOptionPane.showMessageDialog(null, "Masuk Pit!");
            setVisible(false);
            new FrameMokad().setVisible(true);

        }
        if (kl.goldx == playerLocX && kl.goldy == playerLocY) {
            playSoundGold();
            JOptionPane.showMessageDialog(null, "Berhasil dapat gold!");
            setVisible(false);
            new FrameMenang().setVisible(true);

        }
        if (kl.wumpusx == playerLocX && kl.wumpusy == playerLocY) {
            JOptionPane.showMessageDialog(null, "Dimakan Wumpus!");
            playSoundWp();
            setVisible(false);
            new FrameMokad().setVisible(true);

        }
    }

    //Player bergerak
    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            String statusBox = cekStatus(playerLoc);
            JOptionPane.showMessageDialog(null, "Status kotak : " + listKotak.get(playerLoc).getStatus());
            JOptionPane.showMessageDialog(null, "Status di tempat player : " + statusBox);
            JOptionPane.showMessageDialog(null, "Tempat player : " + playerLoc);
            setPriorityTetangga(playerLoc, statusBox);
            giveInfo(playerLoc);
                int newPlayerLoc = makeMovement(playerLoc, statusBox);
                if(newPlayerLoc - 4 == playerLoc){
                    playerLoc = playerLoc + 4;
                    playerLocX =  MoveRight(listKotak.get(newPlayerLoc - 4).getX());
                } else if(newPlayerLoc + 4 == playerLoc){
                    playerLoc = playerLoc - 4;
                    playerLocX =  MoveLeft(listKotak.get(newPlayerLoc + 4).getX());
                } else if(newPlayerLoc - 1 == playerLoc){
                    playerLoc = playerLoc + 1;
                    playerLocY = MoveDown(listKotak.get(newPlayerLoc - 1).getY());
                } else if(newPlayerLoc + 1 == playerLoc){
                    playerLoc = playerLoc - 1;
                    playerLocY = MoveUp(listKotak.get(newPlayerLoc + 1).getY());
                }
        }

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            playSoundWalk();
            colition();
            urlDefPly = "bahan-bahan/Image/player/right_view.png";
            player.setIcon(new ImageIcon(resizeImg(urlDefPly, 100, 100)));
            playerLoc = playerLoc + 4;
            playerLocX = MoveRight(playerLocX);
//            player.arah=0;

        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            playSoundWalk();
            urlDefPly = "bahan-bahan/Image/player/left_view.png";
            player.setIcon(new ImageIcon(resizeImg(urlDefPly, 100, 100)));
            playerLoc = playerLoc - 4;
            playerLocX = MoveLeft(playerLocX);

        }
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            playSoundWalk();
            urlDefPly = "bahan-bahan/Image/player/back_view.png";
            player.setIcon(new ImageIcon(resizeImg(urlDefPly, 100, 100)));
            playerLoc = playerLoc - 1;
            playerLocY = MoveUp(playerLocY);

        }
        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            playSoundWalk();
            urlDefPly = "bahan-bahan/Image/player/front_view.png";
            player.setIcon(new ImageIcon(resizeImg(urlDefPly, 100, 100)));
            playerLoc = playerLoc + 1;
            playerLocY = MoveDown(playerLocY);

        }
        //function cek batas
        border();

        //function cek tile diinjek ga
        darkTile();

        player.setLocation(playerLocX, playerLocY);
        //function dia kena sesuatu ga
        tabrakanObs();

    }

    @Override
    public void keyReleased(KeyEvent e) {
        System.out.println("----------");
        System.out.println("x_player = " + playerLocX);
        System.out.println("y_player = " + playerLocY);
    }

    @Override
    public int MoveUp(int y) {
        y -= 130;
        return y;
    }

    @Override
    public int MoveDown(int y) {
        y += 130;
        return y;
    }

    @Override
    public int MoveLeft(int x) {
        x -= 130;
        return x;
    }

    @Override
    public int MoveRight(int x) {
        x += 130;
        return x;
    }

    public void darkTile() {
        //kolom1
        if (playerLocX == 20 && playerLocY == 20) {
            tg.dark1.setVisible(false);
        }
        if (playerLocX == 20 && playerLocY == 150) {
            tg.dark5.setVisible(false);
        }
        if (playerLocX == 20 && playerLocY == 280) {
            tg.dark9.setVisible(false);
        }
        //kolom2
        if (playerLocX == 150 && playerLocY == 20) {
            tg.dark2.setVisible(false);
        }
        if (playerLocX == 150 && playerLocY == 150) {
            tg.dark6.setVisible(false);
        }
        if (playerLocX == 150 && playerLocY == 280) {
            tg.dark10.setVisible(false);
        }
        if (playerLocX == 150 && playerLocY == 410) {
            tg.dark14.setVisible(false);
        }
        //kolom3
        if (playerLocX == 280 && playerLocY == 20) {
            tg.dark3.setVisible(false);
        }
        if (playerLocX == 280 && playerLocY == 150) {
            tg.dark7.setVisible(false);
        }
        if (playerLocX == 280 && playerLocY == 280) {
            tg.dark11.setVisible(false);
        }
        if (playerLocX == 280 && playerLocY == 410) {
            tg.dark15.setVisible(false);
        }
        //kolom4
        if (playerLocX == 410 && playerLocY == 20) {
            tg.dark4.setVisible(false);
        }
        if (playerLocX == 410 && playerLocY == 150) {
            tg.dark8.setVisible(false);
        }
        if (playerLocX == 410 && playerLocY == 280) {
            tg.dark12.setVisible(false);
        }
        if (playerLocX == 410 && playerLocY == 410) {
            tg.dark16.setVisible(false);
        }

    }

    public void border() {
        //border
        if (playerLocX < 20) {
            playerLocX = 20;
            System.out.println("Nubruk");
        } else if (playerLocX > 410) {
            playerLocX = 410;
            System.out.println("Nubruk");
        }
        if (playerLocY < 20) {
            playerLocY = 20;
            System.out.println("Nubruk");
        } else if (playerLocY > 410) {
            playerLocY = 410;
            System.out.println("Nubruk");
        }
    }

    public void colition() {
        Thread thread = new Thread() {
            public void run() {
                System.out.println("Thread Running");
            }
        };

        thread.start();
    }

    //coba forward chaining
    static Clip walk;

    public static void playSoundWalk() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bahan-bahan\\music\\soundfx\\walk.wav").getAbsoluteFile());
            walk = AudioSystem.getClip();
            walk.open(audioInputStream);
            walk.start();

        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }

    static Clip scream;

    public static void playSoundObs() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bahan-bahan\\music\\soundfx\\scream.wav").getAbsoluteFile());
            scream = AudioSystem.getClip();
            scream.open(audioInputStream);
            scream.start();

        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }

    static Clip goldS;

    public static void playSoundGold() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bahan-bahan\\music\\soundfx\\gold.wav").getAbsoluteFile());
            goldS = AudioSystem.getClip();
            goldS.open(audioInputStream);
            goldS.start();

        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    static Clip wumpusS;

    public static void playSoundWp() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bahan-bahan\\music\\soundfx\\monkeyRoar.wav").getAbsoluteFile());
            wumpusS = AudioSystem.getClip();
            wumpusS.open(audioInputStream);
            wumpusS.start();

        } catch (Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }

    //inisialisasi mati lampu
    //Atribut atribut yang dipakai
    private JPanel mainPanel;
    public static JLabel bkgImg;
    public static JLabel player;
    private JLabel wumpus;
    private JLabel gold;
    private JLabel pit1;
    private JLabel pit2;
    private JLabel pit3;
    private JLabel stench;
    private JLabel breeze;
    private JLabel xfence;
    private JLabel yfence;

    public String cekStatus(int playerLoc){
        String result = null;
        
        if(listKotak.get(playerLoc).getStatus().equals("breeze")){
            result = "breeze, unsafe";
            listKotak.get(playerLoc).setVisited(true);
        } else if(listKotak.get(playerLoc).getStatus().equals("stench")){
            result = "stench, unsafe";
            listKotak.get(playerLoc).setVisited(true);
        } else if(listKotak.get(playerLoc).getStatus().equals("safe")){
            result = "safe";
            listKotak.get(playerLoc).setVisited(true);
        }
        
        return result;
    }
    
    public void setPriorityTetangga(int playerLoc, String currState){
        if(currState.equals("breeze, unsafe")){
            listKotak.get(playerLoc).setPriority(1);
            for(int i = 0 ; i < listKotak.get(playerLoc).tetangga.length ; i++){
                if(!listKotak.get(listKotak.get(playerLoc).tetangga[i]).isVisited()){
                    listKotak.get(listKotak.get(playerLoc).tetangga[i]).setPriority(2);
                }
            }
        } else if(currState.equals("stench, unsafe")){
            listKotak.get(playerLoc).setPriority(1);
            for(int i = 0 ; i < listKotak.get(playerLoc).tetangga.length ; i++){
                if(!listKotak.get(listKotak.get(playerLoc).tetangga[i]).isVisited()){
                    listKotak.get(listKotak.get(playerLoc).tetangga[i]).setPriority(2);
                }
            }
        } else {
            listKotak.get(playerLoc).setPriority(0);
            for(int i = 0 ; i < listKotak.get(playerLoc).tetangga.length ; i++){
                listKotak.get(listKotak.get(playerLoc).tetangga[i]).setPriority(0);
            }
        }
    }
    
    public void giveInfo(int playerLoc){
        
    }
    
    public int makeMovement(int playerLoc, String statusBox){
        int moveTo = 3;
        
        if(statusBox.equals("breeze, unsafe") || statusBox.equals("stench, unsafe")){
            for(int i = 0 ; i < listKotak.get(playerLoc).tetangga.length ; i++){
                if(listKotak.get(listKotak.get(playerLoc).tetangga[i]).getPriority() == 0){
                    moveTo = listKotak.get(playerLoc).tetangga[i];
                } else if(listKotak.get(listKotak.get(playerLoc).tetangga[i]).getPriority() == 1){
                    moveTo = listKotak.get(playerLoc).tetangga[i];
                }
            }
        } else {
            for(int i = 0 ; i < listKotak.get(playerLoc).tetangga.length ; i++){
                if(listKotak.get(listKotak.get(playerLoc).tetangga[i]).getPriority() == 0
                    && !listKotak.get(listKotak.get(playerLoc).tetangga[i]).isVisited()){
                    moveTo = listKotak.get(playerLoc).tetangga[i];
                }
            }
        }
        
        return moveTo;
    }
    
    //panel panel mati
    public static void main(String[] args) {
        new MainAI().setVisible(true);
    }

}
