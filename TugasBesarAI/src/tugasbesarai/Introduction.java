/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import static tugasbesarai.LoginKedua.bgImg;


/**
 *
 * @author natan
 */
public class Introduction extends JLabel {

    public Introduction() {
        initComponents();
    }

    private void initComponents() {
        chat = new String[5];
        chat[0] = "Halo, selamat datang di game WUMPUS!";
        chat[1] = "Perkenalkan namaku TUBES-AI";
        chat[2] = "Bantu aku menemukan GOLD!";
        chat[3] = "Terdapat wumpus dan banyak pit!";
        chat[4] = "Ayo bantu aku cari GOLD bersama/ AI-MODE!";

        //background ke panel
        introMessage = new JLabel(chat[counter]);
        introMessage.setFont(new Font("Calibri", Font.BOLD, 25));
        introMessage.setForeground(Color.WHITE);
        introMessage.setBounds(110, 450, 600, 100);
        introMessage.setVisible(false);
        bgImg.add(introMessage);
        
        player = new JLabel();
        player.setIcon(new ImageIcon(resizeImg("bahan-bahan/Image/player/front_view.png", 65, 65)));
        player.setBounds(20, 460, 65, 65);
        player.setVisible(false);
        bgImg.add(player);
        
        box = new JLabel();
        box.setIcon(new ImageIcon(resizeImg("bahan-bahan/Image/intro/panelChat.png", 600, 180)));
        box.setBounds(10, 330, 600, 260);
        box.setVisible(false);
        bgImg.add(box);

        dim = new JLabel();
        dim.setIcon(new ImageIcon(resizeImg("bahan-bahan/Image/effect/dimmer.png", 600, 600)));
        dim.setBounds(0, 0, 600, 600);
        dim.setVisible(false);
        bgImg.add(dim);

        introBg = new JLabel();
        introBg.setIcon(new ImageIcon(resizeImg("bahan-bahan/Image/begron2.png", 600, 600)));
        introBg.setBounds(0, 0, 600, 600);
        introBg.setVisible(false);
        bgImg.add(introBg);

    }

    //image converter
    private Image resizeImg(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);

        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }

        return dimg;
    }

    //
    public String[] chat;
    public int counter;
    public JLabel introMessage;
    public JLabel player;
    public JLabel box;
    public JLabel dim;
    public JLabel introBg;

}
