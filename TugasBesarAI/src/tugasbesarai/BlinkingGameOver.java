/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import java.awt.Font;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Natasha Yulian
 */
public class BlinkingGameOver extends JLabel implements Runnable {

    public BlinkingGameOver() {
        String begronURL = "bahan-bahan\\Image\\game_over.png";
        Image begron = resizeImage(begronURL, 600, 600);
        this.setIcon(new ImageIcon(resizeImage(begronURL, 600, 600)));
        setBounds(0,0,600,600);
    }

    public Image resizeImage(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        return dimg;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int i = 0;
                while (true) {
                    if (i % 2 == 0) {
                        this.setVisible(true);
                    } else {
                        this.setVisible(false);
                    }
                    i++;
                    Thread.sleep(300);
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
