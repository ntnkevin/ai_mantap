/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Natasha Yulian
 */
public class ForwardChaining {

    public static String tell;
    public static String ask;
    public static ArrayList<String> pengetahuan;

    public static ArrayList<String> facts;
    public static ArrayList<String> clauses;
    public static ArrayList<Integer> count;
    public static ArrayList<String> entailed;

    // to run simply do a: new FC(ask,tell) and then fc.execute()
    // ask is a propositional symbol
    // and tell is a knowledge base 
    // ask : r
    // tell : p=>q;q=>r;p;q;
    public ForwardChaining(String a, String t) {
        pengetahuan = new ArrayList<String>();
        clauses = new ArrayList<String>();
        entailed = new ArrayList<String>();
        facts = new ArrayList<String>();
        count = new ArrayList<Integer>();
        tell = t;
        ask = a;
        init(tell);
    }

    //Cara Forward Chaining nya
    public boolean fcentails() {
    //selama di pengetahuan masi ada ilmu
        while (!pengetahuan.isEmpty()) {
            //ambil elemen pertama terus coba proses
            //diilangin biar sekali aja prosesnya
            String p = pengetahuan.remove(0);
            // add to entailed
            entailed.add(p);
            //untuk setiap klausa
            for (int i = 0; i < clauses.size(); i++) {
                // .... that contain p in its premise
                if (premiseContains(clauses.get(i), p)) {
                    Integer j = count.get(i);
                    // reduce count : unknown elements in each premise
                    count.set(i, --j);
                    // all the elements in the premise are now known
                    if (count.get(i) == 0) {
                        // the conclusion has been proven so put into agenda
                        String head = clauses.get(i).split("=>")[1];
                        // have we just proven the 'ask'?
                        if (head.equals(ask)) {
                            return true;
                        }
                        pengetahuan.add(head);
                    }
                }
            }
        }
        // if we arrive here then ask cannot be entailed
        return false;
    }

    //nanya data awal apa aja yang bisa diketahui
    public static void init(String tell) {
        String[] sentences = tell.split(";");
        for (int i = 0; i < sentences.length; i++) {

            if (!sentences[i].contains("=>")) //kalo bukan premis, berarti fakta
            {
                pengetahuan.add(sentences[i]);// pengetahuan berarti list2 fakta
            } else {
                clauses.add(sentences[i]); //rules
                count.add(sentences[i].split("&").length);
                // ada berapa rules yang di premis tersebut (hitung dari banyak &)
            }
        }
    }

    //ambil premise sama klausa
    //premise => klausa
    //klo ada banyak syarat di pecah2 jadi array
    public boolean premiseContains(String clause, String p) {
        String premise = clause.split("=>")[0];
        String[] conjuncts = premise.split("&");
        // check if p is in the premise
        if (conjuncts.length == 1) {//kalo ga ada &
            return premise.equals(p);
        } else { //kalo banyak syarat2 nya & & &
            return Arrays.asList(conjuncts).contains(p);
        }
    }

    public String execute() {
        String output = "";
        if (fcentails()) {
            // the method returned true so it entails
            output = "YES: ";
            // for each entailed symbol
            for (int i = 0; i < entailed.size(); i++) {
                output += entailed.get(i) + ", ";
            }
            output += ask;
        } else {
            output = "NO";
        }
        return output;
    }

}
