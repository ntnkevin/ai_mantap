/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import java.awt.Font;
import javax.swing.JLabel;

/**
 *
 * @author Natasha Yulian
 */
public class BlinkingText extends JLabel implements Runnable{
     public BlinkingText() {
        this.setText("<html><centre><font color='white'><bold>PRESS ENTER TO CONTINUE</bold></font></centre></html>");
        setFont(new Font("Comic Sans", Font.PLAIN, 40));
        setBounds(155, 240, 300, 100);
    }

    @Override
    public void run() {
        while (true) {
            try {
                int i = 0;
                while (true) {
                    if (i % 2 == 0) {
                        this.setVisible(true);
                    } else {
                        this.setVisible(false);
                    }
                    i++;
                    Thread.sleep(300);
                }    
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
