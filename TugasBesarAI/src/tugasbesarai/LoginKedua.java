/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 *
 * @author Natasha Yulian
 */
public class LoginKedua extends JFrame implements KeyListener {

    public LoginKedua() {
        initComponents();
        addKeyListener(this);
    }

    private void initComponents() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setBounds(600, 600, 600, 600);
        setLocationRelativeTo(null);
        setTitle("Wumpus Game");

        //panel to frame
        bgPanel = new JPanel();
        add(bgPanel);

        //background ke panel
        bgImg = new JLabel();
        bgImg.setSize(600, 600);
        bgImg.setIcon(new ImageIcon(resizeImage("bahan-bahan/Image/begron2.png", 600, 600)));
        bgPanel.add(bgImg);

        //introduction
        intro = new Introduction();

        intro.introMessage.setVisible(true);
        intro.box.setVisible(true);
        intro.player.setVisible(true);
        intro.dim.setVisible(true);
        intro.introBg.setVisible(true);

        btnPlayer = new JLabel();
        btnPlayer.setIcon(new ImageIcon(resizeImage("bahan-bahan/Image/button_menu.png", 200, 80)));
        btnPlayer.setBounds(200, 170, 200, 80);
        bgImg.add(btnPlayer);

        btnAI = new JLabel();
        btnAI.setIcon(new ImageIcon(resizeImage("bahan-bahan/Image/button_menu.png", 200, 80)));
        btnAI.setBounds(200, 310, 200, 80);
        bgImg.add(btnAI);
        
        btnHelp = new JLabel();
        btnHelp.setIcon(new ImageIcon(resizeImage("bahan-bahan/Image/question.png", 50, 50)));
        btnHelp.setBounds(530, 10, 50, 50);
        bgImg.add(btnHelp);

        lblAI = new JLabel("<html><font color='white'><bold>AI</bold></font></html>", SwingConstants.CENTER);
        lblAI.setFont(new Font("Comic Sans", Font.PLAIN, 40));
        lblAI.setBounds(0, 12, 200, 50);
        btnAI.add(lblAI);

        lblPlayer = new JLabel("<html><font color='white'><bold>Player</bold></font></html>", SwingConstants.CENTER);
        lblPlayer.setFont(new Font("Comic Sans", Font.PLAIN, 40));
        lblPlayer.setBounds(0, 12, 200, 50);
        btnPlayer.add(lblPlayer);
        
        lblAIOn = new JLabel();
        lblAIOn.setIcon(new ImageIcon(resizeImage("bahan-bahan/Image/button_menu_select.png", 200, 80)));
        lblAIOn.setBounds(0, 0, 200, 80);
        lblAIOn.setVisible(false);
        btnAI.add(lblAIOn);
        
        lblPlayerOn = new JLabel();
        lblPlayerOn.setIcon(new ImageIcon(resizeImage("bahan-bahan/Image/button_menu_select.png", 200, 80)));
        lblPlayerOn.setBounds(0, 0, 200, 80);
        lblPlayerOn.setVisible(false);
        btnPlayer.add(lblPlayerOn);
        
        
        btnPlayer.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                playSoundClick();
                String kata2 = "Pilih mode Player ? \n"
                               + "Kamu bisa menggerakkan Wilson (character kamu) \n"
                               + "dengan memakai arrow atas bawah kiri kanan. \n"
                               + "Akan ada beberapa HINT untuk jalan ke selanjutnya";
                JOptionPane.showMessageDialog(null,kata2);
                setVisible(false);
                MainPlayer main = new MainPlayer();
                tugasbesarai.LoginPertama.clip.stop();
                playSound();
                main.setVisible(true);
                
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
                lblPlayerOn.setVisible(true);
                playSoundHover();
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
                lblPlayerOn.setVisible(false);
            }
        });
        
        btnAI.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                playSoundClick();
                String kata2 = "Pilih mode AI? \n"
                               + "Di mode ini kamu tinggal nunggu AI nya yang mikir jalan\n"
                               + "Setiap 1 kali jalan sang AI akan berhenti dan kamu bisa tau pemikiran dia seperti apa\n"
                               + "Kalo udah, teken SPASI buat si AI maju lagi. \nMudah Kan? :) ";
                JOptionPane.showMessageDialog(null,kata2);
                
                setVisible(false);
                MainAI main = new MainAI();
                tugasbesarai.LoginPertama.clip.stop();
                playSound();
                main.setVisible(true);
                
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
                lblAIOn.setVisible(true);
                playSoundHover();
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
                lblAIOn.setVisible(false);
            }
        });
        
        btnHelp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                playSoundClick();
                String kata2 = "SELAMAT DATANG DI GAME AI MANTAP \n"
                            + "Tema : WUMPUS\n"
                            + "Misi kamu adalah mencari GOLD PLZ yang sangat tersembunyi di 16 kotak ini\n"
                            + "Penasaran? \nLangsung main aja!!";
                JOptionPane.showMessageDialog(null,kata2);
                
            }
            
            @Override
            public void mouseEntered(MouseEvent e) {
                
            }
            
            @Override
            public void mouseExited(MouseEvent e) {
                
            }
        });
    }

    public Image resizeImage(String url, int w, int h) {
        Image dimg = null;
        try {
            BufferedImage img = ImageIO.read(new File(url));
            dimg = img.getScaledInstance(w, h, Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
        return dimg;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
            //System.out.println("kepencet woi spasinya");
            counterIntro = counterIntro + 1;
            if (counterIntro < 5) { 
                playSoundClick();
                intro.introMessage.setText(intro.chat[counterIntro]);
                intro.setVisible(true);
                intro.introMessage.setVisible(true);
                intro.box.setVisible(true);
                intro.player.setVisible(true);
                intro.dim.setVisible(true);
                intro.introBg.setVisible(true);
            } else {
                intro.setVisible(false);
                intro.introMessage.setVisible(false);
                intro.box.setVisible(false);
                intro.player.setVisible(false);
                intro.dim.setVisible(false);
                intro.introBg.setVisible(false);
            }
            System.out.println("counter++");
        }
    }
    
    static Clip clip2;
    public static void playSound() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bahan-bahan\\music\\theme_song1.wav").getAbsoluteFile());
            clip2 = AudioSystem.getClip();
            clip2.open(audioInputStream);
            clip2.start();
            clip2.loop(Clip.LOOP_CONTINUOUSLY);
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    
    //soundclip effect
    static Clip hover;
    public static void playSoundHover() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bahan-bahan\\music\\soundfx\\hover.wav").getAbsoluteFile());
            hover = AudioSystem.getClip();
            hover.open(audioInputStream);
            hover.start();
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    
    static Clip clicked;
    public static void playSoundClick() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("bahan-bahan\\music\\soundfx\\clicked.wav").getAbsoluteFile());
            clicked = AudioSystem.getClip();
            clicked.open(audioInputStream);
            clicked.start();
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {

    }
    JLabel btnPlayer;
    JLabel btnAI;
    JLabel lblPlayerOn;
    JLabel lblAIOn;
    JLabel lblPlayer;
    JLabel lblAI;
    JLabel btnHelp;
    public JPanel bgPanel;
    public static JLabel bgImg;
    private Introduction intro;
    public int counterIntro = 0;
}
