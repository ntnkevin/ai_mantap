/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tugasbesarai;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/**
 *
 * @author Natasha Yulian
 */
public class FrameMokad extends JFrame{
        
    public FrameMokad(){
        initComponents();
    }
    
    private void initComponents(){
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        //in frame
        this.setBounds(600, 600, 600, 600);
        setLocationRelativeTo(null);
        setTitle("Wumpus Game");
        BlinkingGameOver blinkOver = new BlinkingGameOver();
        this.add(blinkOver);
        Thread t1 = new Thread(blinkOver);
        t1.start();
    }
    
     
}
